# -*- coding: utf-8 -*-
#==============================================================================
#title           :linkedin_profiles_from_google.py
#description     :Uses google search to find linkedin users profiles based on specifc topics 
#                 and extract profiles data 
#author          :Patrick Alves (cpatrickalves@gmail.com)
#date            :12-12-2018
#usage           :python merge_profiles.py File1.csv File2.csv Output.csv
#python_version  :3.6
#==============================================================================

import csv
import paramaters
import re
from parsel import Selector
from time import sleep
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from logzero import logger


# Function that checks each field in the scrapped data
def validate_field(field):    
    if field:
    	# If not None
        return field.strip()    
    else:
    	# If None
        field = ''
    return field

# Function that waits the page to be loaded search for a specific object in the html
def wait_page_loading(xpath_str):
    sel = Selector(text=driver.page_source)
    str_to_looking_for = sel.xpath(xpath_str).extract_first()
    while(str_to_looking_for is None):
        logger.error('waiting ...')
        sleep(0.5)
        sel = Selector(text=driver.page_source)
        str_to_looking_for = sel.xpath(xpath_str).extract_first()

logger.info("Opening the Web browser ...")
# Create selenium webdriver
chromeOptions = webdriver.ChromeOptions()
prefs={"profile.managed_default_content_settings.images": 2,
       "profile.default_content_setting_values.notifications" : 2, 
       'disk-cache-size': 4096 }

#chromeOptions.add_experimental_option('prefs', prefs) 
# Headless mode
if paramaters.useheadless:
    chromeOptions.add_argument('--headless')
chromeOptions.add_argument('--no-proxy-server')

# Opens web browser
driver = webdriver.Chrome(chrome_options=chromeOptions)
# Windows
#driver = webdriver.Chrome("chromedriver.exe",chrome_options=chromeOptions)

# Access the Linkedin
logger.info("Opening Linkedin ...")
driver.get('https://www.linkedin.com/login/pt?fromSignIn=true&trk=guest_homepage-basic_nav-header-signin')

# Login
logger.info("Login with the {} account ...".format(paramaters.linkedin_username))
username = driver.find_element_by_id('username')
username.send_keys(paramaters.linkedin_username)
sleep(0.3)
password = driver.find_element_by_id('password')
password.send_keys(paramaters.linkedin_password)
sleep(0.3)
sign_in_button = driver.find_element_by_xpath('//*[@type="submit"]')
sign_in_button.click()
sleep(0.3)

# Search in google
logger.info("Searching the topics {} in google ...".format(re.findall("\"(.*?)\"",paramaters.search_query)))
driver.get('https://www.google.com')
search_query = driver.find_element_by_name('q')
search_query.send_keys(paramaters.search_query)
search_query.send_keys(Keys.RETURN)

linkedin_urls = []

# Getting the profiles
while(len(linkedin_urls) < paramaters.max_number_of_profiles):
    # Get the search results
    find_linkedin_urls = driver.find_elements_by_tag_name('cite')
    linkedin_urls += [url.text.replace(" › ", "/in/") for url in find_linkedin_urls]

    # Go to the next google page
    driver.find_element_by_xpath('//*[@id="pnnext"]').click()
    sleep(1)

logger.info("Got {} profiles URLs".format(len(linkedin_urls)))

# Saves the data into a CSV file
writer = csv.writer(open(paramaters.file_name, 'w', encoding = 'utf-8'))
writer.writerow(['Name', 'Job Title', 'School', 'Location', 'URL'])

# For each result, access the profile
logger.info("Scraping profiles:")
counter = 1
for linkedin_url in linkedin_urls:

    if counter > paramaters.max_number_of_profiles:
        break

    if not linkedin_url and "..." not in linkedin_url:
        continue

    logger.info("[{}] - {}".format(counter, linkedin_url))
    driver.get(linkedin_url)
    # Wait until the profile name appear
    wait_page_loading('//h1/text()')

    # Create Selector
    sel = Selector(text=driver.page_source)

    # Getting the data
    name = sel.xpath('//li[@class="inline t-24 t-black t-normal break-words"]/text()').extract_first()
    job_title = sel.xpath('//h2[@class="mt1 t-18 t-black t-normal break-words"]/text()').extract_first()
    location = sel.xpath('//li[@class="t-16 t-black t-normal inline-block"]/text()').extract_first()
    linkedin_url = driver.current_url

    # Validate the data
    name = validate_field(name)
    job_title = validate_field(job_title)
    location = validate_field(location)
    linkedin_url = validate_field(linkedin_url)

    # If debug set to True, show the data been scraping
    if paramaters.debug:
        print()
        logger.debug('Name: ' + name)
        logger.debug('Job Title: ' + job_title)
        logger.debug('Location: ' + location)
        logger.debug('URL: ' + linkedin_url + '\n')

    # Saves the data
    writer.writerow([name, job_title, location, linkedin_url])
    counter += 1
    
# Closes the browser
driver.quit()
logger.info("Data saved in the {} file".format(paramaters.file_name))
logger.info("Scraping finished.")