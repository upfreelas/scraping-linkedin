# Max number of profiles
max_number_of_profiles = 5
# Show user data
debug = True
# Use headless Browser
useheadless = False
# Search query
search_query = 'site:linkedin.com/in/ AND "python developer" AND "Brazil"'
# Output file
file_name = 'results_file.csv'

# Linkedin Credentials
linkedin_username = ''
linkedin_password = ''
